$(document).ready(function(){
	$(".ProductsBoxScrollItemList").mCustomScrollbar({
		theme:"dark"
	});
	var owl = $('.owl-MainSlider');
	owl.owlCarousel({
		items:1,
		rtl:true,
		loop:true,
		animateOut: 'fadeOut',
		autoplay:true,
		margin:1,
		autoplayTimeout:2500,
		autoplayHoverPause:true
	});
	var owl = $('.owl-OffSlider');
	owl.owlCarousel({
		items:1,
		rtl:true,
		loop:true,
		animateOut: 'fadeOut',
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:1
			}
		}
	});
	var owl = $('.owl-Products');
	owl.owlCarousel({
		items: 5,
		rtl:true,
		loop:false,
		dots:false,
		autoplay:true,
		nav:true,
		autoplayTimeout:2000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
			0:{
				items:2
			},
			768:{
				items:3
			},
			1000:{
				items:4
			},
			1280:{
				items:6
			}
		}
    });
    $( ".owl-prev").html("<svg fill='currentColor' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' width='24' height='24'><path fill='none' d='M0 0h24v24H0z'/><path d='M13.172 12l-4.95-4.95 1.414-1.414L16 12l-6.364 6.364-1.414-1.414z'/></svg>");
	$( ".owl-next").html("<svg fill='currentColor' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' width='24' height='24'><path fill='none' d='M0 0h24v24H0z'/><path d='M10.828 12l4.95 4.95-1.414 1.414L8 12l6.364-6.364 1.414 1.414z'/></svg>");
	//Go TOp Btn
	function topFunction() {
		document.body.scrollTop = 0; // For Safari
		document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
	}
	//Search
	$(".SearchBtn").click(function() {
		$(this).toggleClass('SearchBtnActive');
		$('.SearchArea').toggleClass('SearchAreaOpen');
	});
	$(".CloseBtnSearchArea i").click(function() {
		$('.SearchBtn').removeClass('SearchBtnActive');
		$('.SearchArea').removeClass('SearchAreaOpen');
	});
	//Mega Menu
	$(".OpenMegaMenuLg").click(function() {
		$('.HkMegeMenu').toggleClass('HkMegeMenuOpen');
		$(this).find('span').toggleClass('active');
	});
	$(document).mouseup(function(e) 
	{
		var container = $(".HkMegeMenu");
		var box2 = $(".OpenMegaMenuLg span");
		if (!container.is(e.target) && container.has(e.target).length === 0 && !box2.is(e.target)) 
		{
			$(".OpenMegaMenuLg").find('span').removeClass('active');
			container.removeClass('HkMegeMenuOpen');
		}
	});
	//MegaMenu Inside
	$('.PrCategoryNav ul li:has(ul)').addClass('HasMegaMenuSub');
	$(".HasMegaMenuSub").hover(function() {
		$(this).toggleClass('HasMegaMenuSubActive');
		$(this).find('ul').toggleClass('MgShow');
	});
});